<?php
require_once("simple_html_dom.php");

function scrap($url)	{
	$curl = curl_init($url);

	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13");
	curl_setopt($curl, CURLOPT_FAILONERROR, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$html = curl_exec($curl);
	curl_close($curl);

	return $html;
}

/*set_time_limit(0);
ini_set('max_execution_time', 60000);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
// php-fpm www.conf edit timeout

/**
 * Plugin Name:       PCS RSS Copy
 * Description:       Copy RSS From all wordpress sites (/feed)
 * Version:           1.7.7
 * Author:            Pham Cong Son
 * License:           GPL-2.0+
 * Text Domain:       pcs-rss-copy
 */
require_once("vendor/autoload.php");
require_once("rss-options.php");
require __DIR__.'/plugin-update-checker-4.4/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/phamcongsonit/pcs-rss-get',
	__FILE__,
	'pcs-rss-copy'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => '2Dca6fTK2VkzDmVHnz',
	'consumer_secret' => 'cnyX5xPVbKFvL7neVPtdYPJknfbBg6rX',
));
//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');


$rssOptions = json_decode(get_option("pcs_rss_option"), true);	

/*register_activation_hook( __FILE__, 'pcs_rss_install' );
function pcs_rss_install(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'pcs_rss_log';
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		url varchar(225) DEFAULT '' NOT NULL,
		categories varchar(225) DEFAULT '' NOT NULL,
		post_status varchar(225) DEFAULT '' NOT NULL,
		author varchar(225) DEFAULT '' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}*/

register_deactivation_hook( __FILE__, 'pcs_rss_uninstall' );
function pcs_rss_uninstall() {
	delete_option("pcs_rss_key");
	 /*global $wpdb;
	 $table_name = $wpdb->prefix . 'pcs_rss_log';
	 $sql = "DROP TABLE IF EXISTS $table_name";
	 $wpdb->query($sql);*/
}  

add_action("wp", "pcs_rss");
function crawl($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$result = curl_exec ($ch);
	curl_close ($ch); 
	return $result;
}
function pcs_rss(){
	if (isset($_GET['pcs_rss'])){
		global $rssOptions;
		if (md5(get_option("pcs_rss_key")) == "9b4d7bcadc3e5406c051557ec0f09ef4"){
			if ($rssOptions['active'] == 'false') exit;
			foreach($rssOptions['data'] as $rssOption){
				
				$rssData = trim(crawl($rssOption['url']));
        preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $rssData);
				try {
					$rss = simplexml_load_string($rssData);
				
				} catch (Exception $e) {
					var_dump($rssOption['url']);
					var_dump($e->getMessage());
					continue;
				}
				foreach($rss->channel->item as $item){
					$title = trim($item->title->__toString());
          var_dump($title);
					$args = array("post_type" => "post", "s" => $title, 'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'));
					$query = query_posts( $args );
					if (have_posts()){
						// exit;
						continue;
					}
					$title = html_entity_decode($title);
					$title = htmlspecialchars_decode($title);
					$args = array("post_type" => "post", "s" => $title, 'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'));
					$query = query_posts( $args );
					if (have_posts()){
						// exit;
						continue;
					}
					$link = trim($item->link->__toString());
					if (strpos($link, "xaluan.com") !== false){
						$html = str_get_html(scrap($link));
                    
						$content = $html->find("#mainContent", 0)->innertext;
					}else{
						$content = $item->{"content:encoded"};
						$content = preg_replace('/The post (.*)./', '', $content);
					}
					
					$html = crawl($link);
					

					preg_match('/<meta property=\"og:image\" content=\"(.*)\"/', $html, $thumbnailMeta);

          if (empty($thumbnailMeta)){
            preg_match('/<meta property=\"og:image\" name=\"xlarge\" content=\"(.*)\"/', $html, $thumbnailMeta);
          }
					$thumbnailURL = "";
					if (isset($thumbnailMeta[1])){
						$thumbnailURL = $thumbnailMeta[1];
					}
					$IAhtml = crawl($link."?ia_markup=1");
					$IAhtml = str_replace("\n", "", $IAhtml);
					$IAhtml = trim(preg_replace('/\t+/', '', $IAhtml));
					preg_match('/<header>(.*)<video>(.*)<source src="(.*)"\/>(.*)<\/video>(.*)<\/header>/', $IAhtml, $videoData);
					if (isset($videoData[3])){
						$videoURL = $videoData[3];
					}else{
						$videoURL = false;
					}
					
					// chua ton tai 
					$my_post = array(
						'post_title'    => $title,
						'post_content'  => $content,
						'post_status'   => $rssOption['post_status'],
						'post_author'   => $rssOption['author'],
						'post_category' => $rssOption['categories']
					);
					$post_id = wp_insert_post( $my_post );
					update_post_meta($post_id, "url_crawl", $link);
					echo "Title: ".$title."\n";
					echo "Thumbnail: ".$thumbnailURL."\n";
					echo "Link: ".$link."\n";
					echo "HTML String Length: ".strlen($html)."\n";
					echo "-----------------------------------------------------";
					// wp_publish_post($post_id);
					if ($thumbnailURL != ""){
						pcsfeatureImage($thumbnailURL, $post_id, $videoURL);
					}

				}
			}
			echo "[".date("d-m-Y H:i:s", time())."] Complete \n";
			exit;
		}else{
			echo "Please active key\n";
			exit;
		}
		
	}
	
}		

function pcsfeatureImage($image_url, $post_id, $videoURL){
	global $rssOptions;
	$upload_dir       = wp_upload_dir(); // Set upload folder
   $image_data       = file_get_contents($image_url); // Get image data
   $unique_file_name = wp_unique_filename( $upload_dir['path'], uniqid()."_".basename($image_url) ); // Generate unique name
   $filename         = basename( $unique_file_name ); // Create image file name
   if( wp_mkdir_p( $upload_dir['path'] ) ) {
	   $file = $upload_dir['path'] . '/' . $filename;
   } else {
	   $file = $upload_dir['basedir'] . '/' . $filename;
   }
   file_put_contents( $file, $image_data );
   $wp_filetype = wp_check_filetype( $filename, null );
   $attachment = array(
	   'post_mime_type' => $wp_filetype['type'],
	   'post_title'     => sanitize_file_name( $filename ),
	   'post_content'   => '',
	   'post_status'    => 'inherit'
   );
   $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
   require_once(ABSPATH . 'wp-admin/includes/image.php');
   $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
   wp_update_attachment_metadata( $attach_id, $attach_data );
   set_post_thumbnail( $post_id, $attach_id );
   if (!$videoURL){
	if ($rssOptions['serverRender'] != null){
		$data = file_get_contents($rssOptions['serverRender']."/api.php?images=".$image_url);
		if ($data){
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
					$fileVideoName = uniqid().".mp4";
					$fileVideo = $upload_dir['path'] . '/' . $fileVideoName;
			} else {
					$fileVideoName = uniqid().".mp4";
					$fileVideo = $upload_dir['basedir'] . '/' . $fileVideoName;
			}
			file_put_contents($fileVideo, $data);
			$wp_filetype = wp_check_filetype( $fileVideoName, null );
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $fileVideoName ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			$attachVideoID = wp_insert_attachment( $attachment, $fileVideo, $post_id );
			update_post_meta($post_id, "header_video", wp_get_attachment_url($attachVideoID));
		}
		
	}
   }else{
	update_post_meta($post_id, "header_video", $videoURL);
   }
}
