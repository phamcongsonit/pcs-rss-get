<?php

add_action( 'admin_menu', 'rss_get_plugin_menu' );

function rss_get_plugin_menu() {
	add_options_page( 
		'Rss Options',
		'PCS Rss Get',
		'manage_options',
		'rss-get.php',
		'rss_get_page'
	);
}
function rss_get_page(){
	if (md5(get_option("pcs_rss_key")) == "9b4d7bcadc3e5406c051557ec0f09ef4"):
		$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$rss_option = get_option("pcs_rss_option", true);
		$rss_option = json_decode($rss_option, true);
	?>
	<style>
		.table{
			width:100%;
			border-collapse: collapse;
			border: 1px solid black;
		}
		th, td {
		    border: 1px solid black;
		    padding:10px;
		}
		.width100{
			width: 100%;
		}
	</style>
	<div class="wrap">
		<h1 class="wp-heading-inline">RSS Get</h1>
		
		
		<?php
		if (isset($_GET['status']) && $_GET['status'] == 'success' ){
			?>
			<div class="notice notice-success is-dismissible">
				Đã cập nhật thành công
			</div>
			<?php
		}
		?>
		<br/>
		<input type="checkbox" class="active" name="active" <?=isset($rss_option['active']) && $rss_option['active']  == 'true' ? 'checked': '' ?>>Kích hoạt lấy bài
		<br/>
		<br/>
		<input type="text" name="serverRender" class="serverRender" value="<?=$rss_option['serverRender']?>" placeholder="Server Render Video">
		<table class="table" style="margin-top:20px;">
			<thead>
				<tr>
					<th style="width:300px">RSS URL</th>
					<th>Category</th>
					<th>Post Status</th>
					<th>Author</th>
					<th style="width:100px">
						<button class="button button-primary button-large" id="add">Thêm</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr class="default-tr" style="display: none">
					<td><input type="text"  name="url" class="width100 url"></td>
					<td>
						<select name="category" id="" class="width100 category" multiple>
							<?php
							$agrs = ["hide_empty" => 0,
			                    "type"      => "post",      
			                    "orderby"   => "name",
			                    "order"     => "ASC"];
							foreach(get_categories($agrs) as $category): ?>
							<option value="<?=$category->term_id?>"><?=$category->name?></option>
							<?php endforeach; ?>
						</select>
					</td>
					<td>
						<select name="post_status" id="" class="width100 post_status">
							<option value="publish">Publish</option>
							<option value="draft">Draft</option>
							<option value="private">Private</option>
						</select>
					</td>
					<td>
						<select name="author" id="" class="width100 author">
							<?php foreach(get_users() as $user): ?>
							<option value="<?=$user->ID?>"><?=$user->display_name?></option>
							<?php endforeach; ?>
						</select>
					</td>
					<td style="text-align: center">
						<button class="button button-secondary button-small delete">Xóa</button>
					</td>
				</tr>
				<?php 
				if (isset($rss_option['data'])):
				foreach($rss_option['data'] as $ro):
				?>
					<tr>
						<td><input type="text"  name="url" class="width100 url" value="<?=$ro['url']?>"></td>
						<td>
							<select name="category" id="" class="width100 category" multiple>
								<?php
								$agrs = ["hide_empty" => 0,
				                    "type"      => "post",      
				                    "orderby"   => "name",
				                    "order"     => "ASC"];
								foreach(get_categories($agrs) as $category): 
									if ($ro['categories'] == ""){
										$ro['categories'] = [];
									}
									?>
								<option value="<?=$category->term_id?>" <?=in_array($category->term_id, $ro['categories']) ? 'selected' : ''?>><?=$category->name?></option>
								<?php endforeach; ?>
							</select>
						</td>
						<td>
							<select name="post_status" id="" class="width100 post_status">
								<option value="publish" <?=$ro['post_status'] == 'publish' ? 'selected' : ''?>>Publish</option>
								<option value="draft" <?=$ro['post_status'] == 'draft' ? 'selected' : ''?>>Draft</option>
								<option value="private" <?=$ro['post_status'] == 'private' ? 'selected' : ''?>>Private</option>
							</select>
						</td>
						<td>
							<select name="author" id="" class="width100 author">
								<?php foreach(get_users() as $user): ?>
								<option value="<?=$user->ID?>" <?=$user->ID == $ro['author'] ? 'selected' : ''?>><?=$user->display_name?></option>
								<?php endforeach; ?>
							</select>
						</td>
						<td style="text-align: center">
							<button class="button button-secondary button-small delete">Xóa</button>
						</td>
					</tr>
				<?php
				endforeach;
				endif; 
				?>
			</tbody>
		</table>
		<div style="margin-top:20px"></div>
		<button class="button button-primary button-large" id="update">Cập nhật</button>
	</div>
	<script type="text/javascript">
		$ = jQuery;
		$(function(){
			
			$("#update").click(function(){
				data = {};
				data.serverRender = $(".serverRender").val();
				data.active = $(".active").is(":checked");
				data.data = [];
				$("table tbody tr:not(.default-tr)").each(function(index, tr){
					var url = ($(tr).find(".url").val());
					var category = ($(tr).find(".category").val());
					var post_status = ($(tr).find(".post_status").val());
					var author = ($(tr).find(".author").val());
					data.data.push({url: url, categories: category, post_status: post_status, author: author})
				})
				$.ajax({
					url: "<?=admin_url( 'admin-ajax.php' )?>",
					type: "POST",
					data: {
						action: "rss_option_ajax",
						data: data
					}
				}).done(function(result){
					window.location.href = '<?= add_query_arg("status", "success", $current_url); ?>' ;
				})
				console.log(data);
			})
			$("#add").click(function(){
				$tr = $("table tbody tr.default-tr").clone();
				$tr.removeClass("default-tr").show();
				$("table tbody").append($tr);
			})
			$(document).on("click", ".delete", function(){
				$(this).closest("tr").remove();
			})
			if ($("table tbody tr").length <= 1){
				$("#add").click();
			}
		})
	</script>
	<?php
	else:
		if (isset($_POST['action']) && $_POST['action'] == 'active'){
			if (md5($_POST['key']) == "9b4d7bcadc3e5406c051557ec0f09ef4"){
				update_option("pcs_rss_key", $_POST['key']);
				echo "<script>window.location.reload()</script>";
			}else{
				$status = "Error, your key not work";
			}
		}
	?>
	<div class="wrap">
		<h1 class="wp-heading-title">Active RSS</h1>
		<div style="padding:20px;background:gainsboro;float:left;">
			<form action="" method="post">
				<input type="hidden" name="action" value="active">
				<input type="text" name="key" placeholder="Key Active">
				<button class="button button-primary">Submit</button>
				
				<?php
				if (isset($status)){
					echo "<p style='color:red'>".$status."</p>";
				}
				?>
			</form>
		</div>
		
	</div>
	<?php
	endif;


}

add_action( 'wp_ajax_rss_option_ajax', 'rss_option_ajax' );
function rss_option_ajax() {
	global $wpdb;
	$data = json_encode($_POST['data']);
	$result = update_option( 'pcs_rss_option', $data, '', 'yes' );
	wp_die();
}